import { Injectable } from "@angular/core";
// import bcp47 from "bcp-47";

// interface ILangTagSchema {
//   language: string;
//   extendedLanguageSubtags: string[];
//   script: string;
//   region: string;
//   variants: string[];
//   extensions: object[];
//   privateuse: string[];
//   irregular: string | null;
//   regular: string | null;
// }

/*
  ISO language tag provider.
*/
// TODO remove? too simple for a service
@Injectable()
export class LangTagProvider {
  /**
   * ISO-639-1 language code
   */
  public get preferredLanguage(): string {
    return this.languageTag.language;
  }

  /**
   * ISO-3166-1 region code
   */
  public get preferredRegion(): string {
    return this.languageTag.region;
  }

  // private languageTag: ILangTagSchema;
  private languageTag: {
    language: string;
    region: string;
  };

  constructor() {
    // this.languageTag = bcp47.parse(navigator.language);
    const [language, region] = navigator.language.split("-");
    this.languageTag = { language, region };
  }
}
