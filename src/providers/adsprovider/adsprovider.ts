import { Injectable } from "@angular/core";
import {
  AdMobFree,
  AdMobFreeBannerConfig,
  AdMobFreeInterstitialConfig,
  // AdMobFreeRewardVideoConfig,
} from "@ionic-native/admob-free";
import { Platform } from "ionic-angular";
import { appConfig } from "../../app-config";
import { HttpClient } from "@angular/common/http";
import { FlurryProvider } from "../flurry/flurry";

interface ICrossPromoResult {
  items: object[];
}

@Injectable()
export class AdmobProvider {
  // Banner Ad Config
  private bannerConfig: AdMobFreeBannerConfig = {
    isTesting: appConfig.admob.isTesting,
    autoShow: false,
    id: appConfig.admob.bannerId,
  };

  // Interstitial Ad's Configurations
  private interstitialConfig: AdMobFreeInterstitialConfig = {
    isTesting: appConfig.admob.isTesting,
    autoShow: false,
    id: appConfig.admob.interstitialId,
  };

  // Reward Video Ad's Configurations
  // RewardVideoConfig: AdMobFreeRewardVideoConfig = {
  //     isTesting: this.testMode,
  //     autoShow: false,
  //     id: this.admobRevarderdId
  // };

  constructor(
    private admobFree: AdMobFree,
    private platform: Platform,
    private http: HttpClient,
    private flurry: FlurryProvider
  ) {
    console.log("Admob provider initializing...");
    this.platform.ready().then(() => {
      this.admobFree.banner.config(this.bannerConfig);

      this.admobFree.interstitial.config(this.interstitialConfig);
      this.admobFree.interstitial.prepare();

      // Load ad configuration
      // this.admobFree.rewardVideo.config(this.RewardVideoConfig);
      // this.admobFree.rewardVideo
      //   .prepare()
      //   .catch(e => console.log("Rewarded ad prepare error: ", e));
    });

    this.admobFree.on("admob.interstitial.events.LOAD_FAIL").subscribe((e) => {
      console.error("Interstitial ad prepare error: ", e);
    });

    this.admobFree.on("admob.interstitial.events.LOAD").subscribe((e) => {
      console.log("Interstitial loaded");
    });

    // Handle interstitial's close event to Prepare Ad again
    this.admobFree.on("admob.interstitial.events.CLOSE").subscribe(() => {
      this.admobFree.interstitial.prepare();
    });

    // Handle Reward's close event to Prepare Ad again
    // this.admobFree.on("admob.rewardvideo.events.CLOSE").subscribe(() => {
    //   this.admobFree.rewardVideo
    //     .prepare()
    //     .then(() => {
    //       console.log("New Rewarded prepared after CLOSE event");
    //     })
    //     .catch(e => console.log("Rewarded prepare after close error: ", e));
    // });
  }

  public async showBanner() {
    this.flurry.logEvent("Show Banner");

    if (!appConfig.admob.isEnabled) {
      return;
    }

    await this.admobFree.banner.prepare();
    this.admobFree.banner.show();
  }

  public showInterstitial() {
    // filter out a fraction of impressions
    if (Math.random() > appConfig.admob.interstitialFrequency) {
      return;
    }

    // send event only if the user will see the ad
    this.flurry.logEvent("Show Interstitial");

    if (!appConfig.admob.isEnabled) {
      return;
    }

    // Check if Ad is loaded
    this.admobFree.interstitial
      .isReady()
      .then(() => {
        // Will show prepared Ad
        this.admobFree.interstitial
          .show()
          .catch((e) => console.log("Interstatial ad show error: " + e));
      })
      .catch((e) => console.log("Interstatial is ready error: " + e));
  }

  // public launchRewarded() {
  //   // Check if Ad is loaded
  //   this.admobFree.rewardVideo
  //     .isReady()
  //     .then(() => {
  //       // Will show prepared Ad
  //       this.admobFree.rewardVideo
  //         .show()
  //         .then(() => {})
  //         .catch((e) => console.log("Rewarded ad show error: " + e));
  //     })
  //     .catch((e) => console.log("Rewarded is ready error: " + e));
  // }

  public async requestApi(): Promise<ICrossPromoResult> {
    let result: ICrossPromoResult = null;

    if (!appConfig.crossPromo.url) {
      return null;
    }

    try {
      result = await this.http
        .get<ICrossPromoResult>(appConfig.crossPromo.url)
        .toPromise();

      console.log("Cross-promo data: ", result);
    } catch (e) {
      console.error("Error requesting cross-promo: ", e);
    }

    return result;
  }
}
