import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable()
export class FavoritesProvider {
  private storageKey = "favorites";

  /**
   * Underlying in-memory array of favorites.
   */
  private idsCache: number[];

  /**
   * Array of favorites tied to Storage and with in-memory cache.
   * Initially the contents are loaded from Storage. After successful
   * loading the contents are served from memory.
   */
  private ids: BehaviorSubject<number[]>;

  constructor(private storage: Storage) {
    console.log("Initializing FavoritesProvider");

    // no favorites by default
    this.ids = new BehaviorSubject([]);

    // get favorites from Storage and emit them along with some messages
    Observable.fromPromise(this.storage.get(this.storageKey)).subscribe(
      (ids) => {
        this.idsCache = ids || [];
        console.log(
          `FavoritesProvider: ${this.idsCache.length} items loaded from Storage`,
        );
      },
      (err) => {
        console.error(
          `FavoritesProvider: unable to load data! ${err}\nUsing empty cache...`,
        );
        this.idsCache = [];
      },
      () => {
        this.ids.next(this.idsCache);
      },
    );
  }

  /**
   * Check if movie is favorite
   * @param movieId movie id
   */
  public isFavorite(movieId: number) {
    return this.ids.map((ids) => {
      const isFavorite = ids.indexOf(movieId) !== -1;
      console.log(
        `Movie ${movieId} ${isFavorite ? "is" : "is not"} in favorites`,
      );
      return isFavorite;
    });
  }

  /**
   * Get ids of all favorite movies.
   */
  public getAllIds() {
    return this.ids.map((ids) => [...ids]);
  }

  /**
   * Add movie to favorites by id.
   * @param movieId movie id
   * @returns whether the movie was added to favorites
   */
  public add(movieId: number) {
    return this.isFavorite(movieId)
      .take(1)
      .map((isFavorite) => {
        if (!isFavorite) {
          console.log(`Adding movie ${movieId} to favorites`);
          this.idsCache.push(movieId);
          this.ids.next(this.idsCache);
          this.save();
        }

        return !isFavorite;
      })
      .toPromise();
  }

  /**
   * Remove movie from favorites by id.
   * @param movieId movie id
   * @returns whether the movie was removed from favorites
   */
  public remove(movieId: number) {
    return this.isFavorite(movieId)
      .take(1)
      .map((isFavorite) => {
        if (isFavorite) {
          console.log(`Removing movie ${movieId} from favorites`);
          const i = this.idsCache.indexOf(movieId);
          this.idsCache.splice(i, 1);
          this.ids.next(this.idsCache);
          this.save();
        }

        return isFavorite;
      })
      .toPromise();
  }

  private async save() {
    await this.storage.set(this.storageKey, this.idsCache);
    console.log("FavoritesProvider: data successfully saved");
  }
}
