import { Injectable } from "@angular/core";
import { SocialSharing } from "@ionic-native/social-sharing";
import {
  MoviesDataProvider,
  IMovieInfoObject
} from "../movies-data/movies-data";
import { TranslateService } from "@ngx-translate/core";
import { forkJoin } from "rxjs/observable/forkJoin";
import { appConfig } from "../../app-config";
import { mergeMap } from "rxjs/operators";

@Injectable()
export class SocialSharingProvider {
  constructor(
    private translate: TranslateService,
    private movies: MoviesDataProvider,
    private socialSharing: SocialSharing
  ) {}

  public shareMovie(movieId: number) {
    console.log(`Sharing movie ${movieId}`);

    const translation = this.translate.get([
      "social-sharing-service.share-movie.subject",
      "social-sharing-service.share-movie.message"
    ]);

    const movieDetails = this.movies.movieInfo({ id: movieId });

    return forkJoin([translation, movieDetails])
      .pipe(
        mergeMap(([t, movieResult]) => {
          const movie = movieResult as IMovieInfoObject;
          const posterUrl = this.movies.backdropUrl(
            movie.poster_path,
            "original"
          );

          return this.socialSharing.shareWithOptions({
            subject: t["social-sharing-service.share-movie.subject"],
            message: t["social-sharing-service.share-movie.message"],
            files: [posterUrl],
            url: appConfig.socialSharing.shareMovieUrl
          });
        })
      )
      .toPromise();
  }
}
