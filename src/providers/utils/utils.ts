import { Injectable } from "@angular/core";
import { AlertController } from "ionic-angular";
import { Market } from "@ionic-native/market";
import { AppAvailability } from "@ionic-native/app-availability";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class UtilsProvider {
  constructor(
    private alertCtrl: AlertController,
    private market: Market,
    private appAvailability: AppAvailability,
    private translate: TranslateService,
  ) {}

  public promptInstallYoutube() {
    this.translate
      .get([
        "utils-service.youtube-prompt.title",
        "utils-service.youtube-prompt.message",
        "utils-service.youtube-prompt.install-button",
      ])
      .subscribe((t) => {
        const confirm = this.alertCtrl.create({
          title: t["utils-service.youtube-prompt.title"],
          message: t["utils-service.youtube-prompt.message"],
          buttons: [
            {
              text: t["utils-service.youtube-prompt.install-button"],
              handler: () => {
                this.market.open("com.google.android.youtube");
              },
            },
          ],
        });
        confirm.present();
      });
  }

  public checkYoutubeInstalled() {
    return this.appAvailability.check("com.google.android.youtube");
  }

  /**
   * Returns thumbnail url for a given YouTube video
   * @param videoId video id
   */
  public getYoutubeThumbnail(videoId) {
    return "https://i.ytimg.com/vi/" + videoId + "/hqdefault.jpg";
  }
}
