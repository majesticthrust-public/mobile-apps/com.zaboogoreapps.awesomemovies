import { Injectable } from "@angular/core";
import { AppRate } from "@ionic-native/app-rate";
import { FlurryProvider } from "../flurry/flurry";
import { EmailComposer } from "@ionic-native/email-composer";
import { appConfig } from "../../app-config";
import { Platform } from "ionic-angular";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { LangTagProvider } from "../lang-tag/lang-tag";

@Injectable()
export class ApprateProvider {
  constructor(
    private platform: Platform,
    private appRate: AppRate,
    private email: EmailComposer,
    private androidPermissions: AndroidPermissions,
    private langTag: LangTagProvider
  ) {
    this.platform.ready().then(() => {
      this.setupAppRate();
    });
  }

  public promptForRating(immediately: boolean) {
    this.appRate.promptForRating(immediately);
  }

  private setupAppRate() {
    this.appRate.preferences = {
      storeAppURL: {
        android: `market://details?id=${appConfig.app.packageName}`
      },
      usesUntilPrompt: appConfig.rateDialog.usesUntilPrompt,
      promptAgainForEachNewVersion:
        appConfig.rateDialog.promptAgainForEachNewVersion,
      useLanguage: this.langTag.preferredLanguage
    };

    this.appRate.preferences.callbacks = {
      handleNegativeFeedback: () => {
        this.composeEmail();
      }
    };
  }

  private async composeEmail() {
    if (this.platform.is("cordova")) {
      await this.androidPermissions.requestPermissions([
        // this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.GET_ACCOUNTS
      ]);
    }

    // https://github.com/ionic-team/ionic-native/issues/3070
    // const isAvailable = await this.emailComposer.isAvailable();
    const isAvailable = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.GET_ACCOUNTS
    );

    if (!isAvailable) {
      throw new Error("Email composer unavailable!");
    }

    this.email.open({
      to: appConfig.rateDialog.developerEmail,
      subject: "Feedback regarding Awesome Movies app",
      body: `Package name: ${appConfig.app.packageName}`
    });
  }
}
