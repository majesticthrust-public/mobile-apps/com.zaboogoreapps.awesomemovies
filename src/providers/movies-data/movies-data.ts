import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { appConfig } from "../../app-config";
import MovieDB from "moviedb-promise";
import { LangTagProvider } from "../lang-tag/lang-tag";
import jsonStringify from "fast-json-stable-stringify";

/**
 * TMDB api configuration method result
 */
export const tmdbConfiguration = {
  images: {
    base_url: "http://image.tmdb.org/t/p/",
    secure_base_url: "https://image.tmdb.org/t/p/",
    backdrop_sizes: ["w300", "w780", "w1280", "original"],
    logo_sizes: ["w45", "w92", "w154", "w185", "w300", "w500", "original"],
    poster_sizes: ["w92", "w154", "w185", "w342", "w500", "w780", "original"],
    profile_sizes: ["w45", "w185", "h632", "original"],
    still_sizes: ["w92", "w185", "w300", "original"]
  },
  change_keys: [
    "adult",
    "air_date",
    "also_known_as",
    "alternative_titles",
    "biography",
    "birthday",
    "budget",
    "cast",
    "certifications",
    "character_names",
    "created_by",
    "crew",
    "deathday",
    "episode",
    "episode_number",
    "episode_run_time",
    "freebase_id",
    "freebase_mid",
    "general",
    "genres",
    "guest_stars",
    "homepage",
    "images",
    "imdb_id",
    "languages",
    "name",
    "network",
    "origin_country",
    "original_name",
    "original_title",
    "overview",
    "parts",
    "place_of_birth",
    "plot_keywords",
    "production_code",
    "production_companies",
    "production_countries",
    "releases",
    "revenue",
    "runtime",
    "season",
    "season_number",
    "season_regular",
    "spoken_languages",
    "status",
    "tagline",
    "title",
    "translations",
    "tvdb_id",
    "tvrage_id",
    "type",
    "video",
    "videos"
  ]
};

/**
 * Methods of the moviedb-promise library. Needed for proxying.
 */
//#region
const moviedbMethods = [
  "configuration",
  "find",
  "searchMovie",
  "searchTv",
  "searchMulti",
  "searchCollection",
  "searchPerson",
  "searchList",
  "searchCompany",
  "searchKeyword",
  "collectionInfo",
  "collectionImages",
  "discoverMovie",
  "discoverTv",
  "movieInfo",
  "movieAlternativeTitles",
  "movieCredits",
  "movieExternalIds",
  "movieImages",
  "movieVideos",
  "movieKeywords",
  "movieRecommendations",
  "movieReleases",
  "movieReleaseDates",
  "movieTrailers",
  "movieTranslations",
  "movieSimilar",
  "movieReviews",
  "movieLists",
  "movieChanges",
  "movieRatingUpdate",
  "tvInfo",
  "tvCredits",
  "tvExternalIds",
  "tvImages",
  "tvVideos",
  "tvSimilar",
  "tvTranslations",
  "tvSeasonInfo",
  "tvSeasonCredits",
  "tvSeasonVideos",
  "tvSeasonExternalIds",
  "tvSeasonImages",
  "tvEpisodeInfo",
  "tvEpisodeCredits",
  "tvEpisodeExternalIds",
  "tvEpisodeImages",
  "tvOnTheAir",
  "tvAiringToday",
  "tvRecommend",
  "tvChanges",
  "personInfo",
  "personMovieCredits",
  "personTvCredits",
  "personCombinedCredits",
  "personImages",
  "personTaggedImages",
  "personChanges",
  "personLatest",
  "personPopular",
  "personExternalIds",
  "creditInfo",
  "listInfo",
  "genreMovieList",
  "genreTvList",
  "genreMovies",
  "keywordInfo",
  "keywordMovies",
  "companyInfo",
  "companyMovies",
  "accountInfo",
  "accountLists",
  "accountFavoriteMovies",
  "accountFavoriteUpdate",
  "accountRatedMovies",
  "accountMovieWatchlist",
  "accountTvWatchlist",
  "accountWatchlistUpdate",
  "accountRatedTv",
  "accountFavoriteTv",
  "miscLatestMovies",
  "miscUpcomingMovies",
  "miscNowPlayingMovies",
  "miscPopularMovies",
  "miscTopRatedMovies",
  "miscChangedMovies",
  "miscChangedTvs",
  "miscChangedPeople",
  "miscTopRatedTvs",
  "miscPopularTvs",
  "requestToken",
  "session"
];
//#endregion

export interface ITMDBRequestError {
  status_message: string;
  status_code: number;
}

export function isTMDBRequestError(
  data: object | ITMDBRequestError
): data is ITMDBRequestError {
  const err = data as ITMDBRequestError;
  return err.status_code !== undefined && err.status_message !== undefined;
}

export interface IDateRange {
  maximum: string;
  minimum: string;
}

export interface IGenre {
  id: number;
  name: string;
}

export interface IMovieListResultObject {
  poster_path: string | null;
  adult: boolean;
  overview: string;
  release_date: string;
  genre_ids: number[];
  id: number;
  original_title: string;
  original_language: string;
  title: string;
  backdrop_path: string | null;
  popularity: number;
  vote_count: number;
  video: boolean;
  vote_averate: number;
}

export interface IMovieInfoObject {
  adult: boolean;
  backdrop_path: string | null;
  belongs_to_collection: null | object;
  budget: number;
  genres: IGenre[];
  homepage: string;
  id: number;
  imdb_id: string | null;
  original_language: string;
  original_title: string;
  overview: string | null;
  popularity: number;
  poster_path: string | null;
  production_companies: Array<{
    name: string;
    id: number;
    logo_path: string | null;
    origin_country: string;
  }>;
  production_countries: Array<{
    iso_3166_1: string;
    name: string;
  }>;
  release_date: string;
  revenue: number;
  runtime: number | null;
  spoken_languages: Array<{
    iso_639_1: string;
    name: string;
  }>;
  status:
    | "Rumored"
    | "Planned"
    | "In Production"
    | "Post Production"
    | "Released"
    | "Canceled";
  tagline: string | null;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface IVideoObject {
  id: string;
  /** Language code */
  iso_639_1: string;
  /** Country code */
  iso_3166_1: string;
  /** Video hosting site video key */
  key: string;
  /** Video name */
  name: string;
  /** Video hosting site, e.g. YouTube, or Vimeo */
  site: string;
  size: 360 | 480 | 720 | 1080;
  type:
    | "Trailer"
    | "Teaser"
    | "Clip"
    | "Featurette"
    | "Behind the Scenes"
    | "Bloopers";
}

export interface IDiscoverMovieParams {
  sort_by?:
    | "popularity.asc"
    | "popularity.desc"
    | "release_date.asc"
    | "release_date.desc"
    | "revenue.asc"
    | "revenue.desc"
    | "primary_release_date.asc"
    | "primary_release_date.desc"
    | "original_title.asc"
    | "original_title.desc"
    | "vote_average.asc"
    | "vote_average.desc"
    | "vote_count.asc"
    | "vote_count.desc";

  certification_country?: string;
  certification?: string;
  "certification.lte"?: string;
  "certification.gte"?: string;
  include_adult?: boolean;
  include_video?: boolean;
  page?: number;
  primary_release_year?: number;
  "primary_release_date.gte"?: string;
  "primary_release_date.lte"?: string;
  "release_date.gte"?: string;
  "release_date.lte"?: string;
  with_release_type?: number;
  year?: number;
  "vote_count.gte"?: number;
  "vote_count.lte"?: number;
  "vote_average.gte"?: number;
  "vote_average.lte"?: number;
  with_cast?: string;
  with_crew?: string;
  with_people?: string;
  with_companies?: string;
  with_genres?: string;
  without_genres?: string;
  with_keywords?: string;
  without_keywords?: string;
  "with_runtime.gte"?: number;
  "with_runtime.lte"?: number;
  with_original_language?: string;
}

interface ICached<T> {
  cache: T;
  timestamp: number;
}

// tslint:disable-next-line:interface-name
export interface MoviesDataProvider {
  configuration(params: object): Promise<object | ITMDBRequestError>;
  find(params: object): Promise<object | ITMDBRequestError>;
  searchMovie(params: {
    query: string;
    page?: number;
    include_adult?: boolean;
    year?: number;
    primary_release_year?: number;
  }): Promise<
    | {
        page: number;
        results: IMovieListResultObject[];
        total_results: number;
        total_pages: number;
      }
    | ITMDBRequestError
  >;
  searchTv(params: object): Promise<object | ITMDBRequestError>;
  searchMulti(params: object): Promise<object | ITMDBRequestError>;
  searchCollection(params: object): Promise<object | ITMDBRequestError>;
  searchPerson(params: object): Promise<object | ITMDBRequestError>;
  searchList(params: object): Promise<object | ITMDBRequestError>;
  searchCompany(params: object): Promise<object | ITMDBRequestError>;
  searchKeyword(params: object): Promise<object | ITMDBRequestError>;
  collectionInfo(params: object): Promise<object | ITMDBRequestError>;
  collectionImages(params: object): Promise<object | ITMDBRequestError>;
  discoverMovie(
    params: IDiscoverMovieParams
  ): Promise<
    | {
        page: number;
        results: IMovieListResultObject[];
        total_results: number;
        total_pages: number;
      }
    | ITMDBRequestError
  >;
  discoverTv(params: object): Promise<object | ITMDBRequestError>;
  movieInfo(params: {
    id: number;
  }): Promise<IMovieInfoObject | ITMDBRequestError>;
  movieAlternativeTitles(params: object): Promise<object | ITMDBRequestError>;
  movieCredits(params: object): Promise<object | ITMDBRequestError>;
  movieExternalIds(params: object): Promise<object | ITMDBRequestError>;
  movieImages(params: object): Promise<object | ITMDBRequestError>;
  movieVideos(params: {
    id: number;
  }): Promise<
    | {
        id: number;
        results: IVideoObject[];
      }
    | ITMDBRequestError
  >;
  movieKeywords(params: object): Promise<object | ITMDBRequestError>;
  movieRecommendations(params: object): Promise<object | ITMDBRequestError>;
  movieReleases(params: object): Promise<object | ITMDBRequestError>;
  movieReleaseDates(params: object): Promise<object | ITMDBRequestError>;
  movieTrailers(params: object): Promise<object | ITMDBRequestError>;
  movieTranslations(params: object): Promise<object | ITMDBRequestError>;
  movieSimilar(params: object): Promise<object | ITMDBRequestError>;
  movieReviews(params: object): Promise<object | ITMDBRequestError>;
  movieLists(params: object): Promise<object | ITMDBRequestError>;
  movieChanges(params: object): Promise<object | ITMDBRequestError>;
  movieRatingUpdate(params: object): Promise<object | ITMDBRequestError>;
  tvInfo(params: object): Promise<object | ITMDBRequestError>;
  tvCredits(params: object): Promise<object | ITMDBRequestError>;
  tvExternalIds(params: object): Promise<object | ITMDBRequestError>;
  tvImages(params: object): Promise<object | ITMDBRequestError>;
  tvVideos(params: object): Promise<object | ITMDBRequestError>;
  tvSimilar(params: object): Promise<object | ITMDBRequestError>;
  tvTranslations(params: object): Promise<object | ITMDBRequestError>;
  tvSeasonInfo(params: object): Promise<object | ITMDBRequestError>;
  tvSeasonCredits(params: object): Promise<object | ITMDBRequestError>;
  tvSeasonVideos(params: object): Promise<object | ITMDBRequestError>;
  tvSeasonExternalIds(params: object): Promise<object | ITMDBRequestError>;
  tvSeasonImages(params: object): Promise<object | ITMDBRequestError>;
  tvEpisodeInfo(params: object): Promise<object | ITMDBRequestError>;
  tvEpisodeCredits(params: object): Promise<object | ITMDBRequestError>;
  tvEpisodeExternalIds(params: object): Promise<object | ITMDBRequestError>;
  tvEpisodeImages(params: object): Promise<object | ITMDBRequestError>;
  tvOnTheAir(params: object): Promise<object | ITMDBRequestError>;
  tvAiringToday(params: object): Promise<object | ITMDBRequestError>;
  tvRecommend(params: object): Promise<object | ITMDBRequestError>;
  tvChanges(params: object): Promise<object | ITMDBRequestError>;
  personInfo(params: object): Promise<object | ITMDBRequestError>;
  personMovieCredits(params: object): Promise<object | ITMDBRequestError>;
  personTvCredits(params: object): Promise<object | ITMDBRequestError>;
  personCombinedCredits(params: object): Promise<object | ITMDBRequestError>;
  personImages(params: object): Promise<object | ITMDBRequestError>;
  personTaggedImages(params: object): Promise<object | ITMDBRequestError>;
  personChanges(params: object): Promise<object | ITMDBRequestError>;
  personLatest(params: object): Promise<object | ITMDBRequestError>;
  personPopular(params: object): Promise<object | ITMDBRequestError>;
  personExternalIds(params: object): Promise<object | ITMDBRequestError>;
  creditInfo(params: object): Promise<object | ITMDBRequestError>;
  listInfo(params: object): Promise<object | ITMDBRequestError>;
  genreMovieList(): Promise<{ genres: IGenre[] } | ITMDBRequestError>;
  genreTvList(): Promise<{ genres: IGenre[] } | ITMDBRequestError>;
  genreMovies(params: object): Promise<object | ITMDBRequestError>;
  keywordInfo(params: object): Promise<object | ITMDBRequestError>;
  keywordMovies(params: object): Promise<object | ITMDBRequestError>;
  companyInfo(params: object): Promise<object | ITMDBRequestError>;
  companyMovies(params: object): Promise<object | ITMDBRequestError>;
  accountInfo(params: object): Promise<object | ITMDBRequestError>;
  accountLists(params: object): Promise<object | ITMDBRequestError>;
  accountFavoriteMovies(params: object): Promise<object | ITMDBRequestError>;
  accountFavoriteUpdate(params: object): Promise<object | ITMDBRequestError>;
  accountRatedMovies(params: object): Promise<object | ITMDBRequestError>;
  accountMovieWatchlist(params: object): Promise<object | ITMDBRequestError>;
  accountTvWatchlist(params: object): Promise<object | ITMDBRequestError>;
  accountWatchlistUpdate(params: object): Promise<object | ITMDBRequestError>;
  accountRatedTv(params: object): Promise<object | ITMDBRequestError>;
  accountFavoriteTv(params: object): Promise<object | ITMDBRequestError>;
  miscLatestMovies(params: object): Promise<object | ITMDBRequestError>;
  miscUpcomingMovies(params: {
    page: number;
  }): Promise<
    | {
        page: number;
        results: IMovieListResultObject;
        dates: IDateRange;
        total_pages: number;
        total_results: number;
      }
    | ITMDBRequestError
  >;
  miscNowPlayingMovies(params: {
    page: number;
  }): Promise<
    | {
        page: number;
        results: IMovieListResultObject;
        dates: IDateRange;
        total_pages: number;
        total_results: number;
      }
    | ITMDBRequestError
  >;
  miscPopularMovies(params: {
    page: number;
  }): Promise<
    | {
        page: number;
        results: IMovieListResultObject;
        total_pages: number;
        total_results: number;
      }
    | ITMDBRequestError
  >;
  miscTopRatedMovies(params: {
    page: number;
  }): Promise<
    | {
        page: number;
        results: IMovieListResultObject;
        total_pages: number;
        total_results: number;
      }
    | ITMDBRequestError
  >;
  miscChangedMovies(params: object): Promise<object | ITMDBRequestError>;
  miscChangedTvs(params: object): Promise<object | ITMDBRequestError>;
  miscChangedPeople(params: object): Promise<object | ITMDBRequestError>;
  miscTopRatedTvs(params: object): Promise<object | ITMDBRequestError>;
  miscPopularTvs(params: object): Promise<object | ITMDBRequestError>;
  requestToken(params: object): Promise<object | ITMDBRequestError>;
  session(params: object): Promise<object | ITMDBRequestError>;
}

@Injectable()
export class MoviesDataProvider {
  public moviedb: MovieDB;

  private storagePrefix = "__movies-data_request_cache";

  constructor(private langTag: LangTagProvider, private storage: Storage) {
    this.moviedb = new MovieDB(appConfig.tmdb.apiKey);

    // proxy moviedb-promise method calls
    for (const method of moviedbMethods) {
      this[method] = params => this.callTMDB(method, params);
    }
  }

  public logoUrl(path: string, size: string) {
    if (tmdbConfiguration.images.logo_sizes.indexOf(size) !== -1) {
      return tmdbConfiguration.images.secure_base_url + size + path;
    } else {
      throw new Error(
        `Invalid size "${size}"! Valid sizes: ${tmdbConfiguration.images.logo_sizes.toString()}`
      );
    }
  }

  public backdropUrl(path: string, size: string) {
    if (tmdbConfiguration.images.backdrop_sizes.indexOf(size) !== -1) {
      return tmdbConfiguration.images.secure_base_url + size + path;
    } else {
      throw new Error(
        `Invalid size "${size}"! Valid sizes: ${tmdbConfiguration.images.backdrop_sizes.toString()}`
      );
    }
  }

  public posterUrl(path: string, size: string) {
    if (tmdbConfiguration.images.poster_sizes.indexOf(size) !== -1) {
      return tmdbConfiguration.images.secure_base_url + size + path;
    } else {
      throw new Error(
        `Invalid size "${size}"! Valid sizes: ${tmdbConfiguration.images.poster_sizes.toString()}`
      );
    }
  }

  public profileUrl(path: string, size: string) {
    if (tmdbConfiguration.images.profile_sizes.indexOf(size) !== -1) {
      return tmdbConfiguration.images.secure_base_url + size + path;
    } else {
      throw new Error(
        `Invalid size "${size}"! Valid sizes: ${tmdbConfiguration.images.profile_sizes.toString()}`
      );
    }
  }

  public stillUrl(path: string, size: string) {
    if (tmdbConfiguration.images.still_sizes.indexOf(size) !== -1) {
      return tmdbConfiguration.images.secure_base_url + size + path;
    } else {
      throw new Error(
        `Invalid size "${size}"! Valid sizes: ${tmdbConfiguration.images.still_sizes.toString()}`
      );
    }
  }

  private async callTMDB(method: string, params: object) {
    console.log("Requesting TMDB", method, params);

    // database key
    const key =
      this.storagePrefix +
      jsonStringify({
        method,
        params
      });

    let stored: ICached<object>;

    // try to retrieve from cache
    stored = await this.storage.get(key);
    if (stored) {
      const cacheAge = Date.now() - stored.timestamp;
      if (cacheAge < appConfig.tmdb.cacheTTL) {
        console.log(
          "TMDB",
          method,
          params,
          `cache hit. Cache still valid (age: ${cacheAge}, max: ${appConfig.tmdb.cacheTTL})`
        );
        return stored.cache;
      } else {
        console.log(
          "TMDB",
          method,
          params,
          `cache hit. Cache expired (age: ${cacheAge}, max: ${appConfig.tmdb.cacheTTL})`
        );
      }
    }

    // save to cache and return

    const result = await this.moviedb[method]({
      language: this.langTag.preferredLanguage,
      region: this.langTag.preferredRegion,
      ...params
    });

    stored = {
      cache: result,
      timestamp: Date.now()
    };

    this.storage.set(key, stored);
    return result;
  }
}
