/**
 * Function that returns a page of arbitrary content.
 * Returns a promised array of objects for a given page. If no content is
 * available for given page, returns an empty array.
 * @param {number} page page number, starting from 1.
 * @returns {Promise<T[]>} promised array with content, or empty array.
 */
export type IPagedRequest<T> = (page: number) => Promise<T[]>;

/**
 * An adapter that converts paginated content to array-like.
 * Content is requested and cached on the go.
 */
export class AntiPaginationAdapter<T> {
  private objectCache: T[] = [];
  private page: number = 1;

  constructor(private pagedRequest: IPagedRequest<T>) {}

  /**
   * Return up to `count` items with offset `offset`.
   * @param offset
   * @param count
   */
  public async get(offset: number = 0, count: number): Promise<T[]> {
    while (this.objectCache.length < offset + count) {
      const newItems = await this.pagedRequest(this.page);
      this.page += 1;

      if (newItems.length > 0) {
        this.objectCache.push(...newItems);
      } else {
        break;
      }
    }

    return this.objectCache.slice(offset, offset + count);
  }
}
