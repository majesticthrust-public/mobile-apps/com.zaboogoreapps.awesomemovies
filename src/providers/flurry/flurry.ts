import { Injectable } from "@angular/core";
import { appConfig } from "../../app-config";
import {
  FlurryAnalytics,
  FlurryAnalyticsObject,
} from "@ionic-native/flurry-analytics";
import { Storage } from "@ionic/storage";
import uuidv4 from "uuid/v4";
import { ReplaySubject } from "rxjs";

@Injectable()
export class FlurryProvider {
  private fa = new ReplaySubject<FlurryAnalyticsObject>(1);

  private storagePrefix = "flurry__";

  constructor(private flurry: FlurryAnalytics, private storage: Storage) {
    if (appConfig.flurry.trackEvents) {
      this.init();
    } else {
      console.log(
        "Flurry: event tracking is disabled! All events will only be printed to console."
      );
    }
  }

  public logEvent(event: string, params?: object) {
    console.log(`Flurry: tracking event '${event}'`, params);

    if (appConfig.flurry.trackEvents) {
      this.fa.subscribe((fa) =>
        fa
          .logEvent(event, params)
          .catch((e) => console.error("Flurry logEvent error:", e))
      );
    }
  }

  public startTimedEvent(event: string, params?: object) {
    console.log(`Flurry: start timed event '${event}'`, params);

    if (appConfig.flurry.trackEvents) {
      this.fa.subscribe((fa) =>
        fa
          .startTimedEvent(event, params)
          .catch((e) => console.error("Flurry startTimedEvent error:", e))
      );
    }
  }

  public endTimedEvent(event: string, params?: object) {
    console.log(`Flurry: end timed event '${event}'`, params);

    if (appConfig.flurry.trackEvents) {
      this.fa.subscribe((fa) =>
        fa
          .endTimedEvent(event, params)
          .catch((e) => console.error("Flurry endTimedEvent error:", e))
      );
    }
  }

  private async init() {
    let userId: string;

    try {
      userId = await this.storage.get(this.storagePrefix + "userId");

      if (!userId) {
        console.log("Flurry: no userId is stored! Generating...");

        userId = uuidv4();
        this.storage.set(this.storagePrefix + "userId", userId);
      }
    } finally {
      console.log(`Flurry: userId ${userId}`);

      const fa = this.flurry.create({
        appKey: appConfig.flurry.apiKey,
        reportSessionsOnClose: true,
        enableLogging: true,
        userId,
      });

      this.fa.next(fa);
    }
  }
}
