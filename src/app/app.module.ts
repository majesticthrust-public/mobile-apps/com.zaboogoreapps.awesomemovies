import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule } from "@ionic/storage";
import { AdMobFree } from "@ionic-native/admob-free";
import { FlurryAnalytics } from "@ionic-native/flurry-analytics";
import { AppAvailability } from "@ionic-native/app-availability";
import { YoutubeVideoPlayer } from "@ionic-native/youtube-video-player";
import { Market } from "@ionic-native/market";
import { Globalization } from "@ionic-native/globalization";
import { SocialSharing } from "@ionic-native/social-sharing";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { AppRate } from "@ionic-native/app-rate";
import { EmailComposer } from "@ionic-native/email-composer";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { HTTP } from "@ionic-native/http";

import { MyApp } from "./app.component";
import { AdmobProvider } from "../providers/adsprovider/adsprovider";
import { UtilsProvider } from "../providers/utils/utils";
import { MoviesDataProvider } from "../providers/movies-data/movies-data";
import { LangTagProvider } from "../providers/lang-tag/lang-tag";
import { FavoritesProvider } from "../providers/favorites/favorites";
import { FlurryProvider } from "../providers/flurry/flurry";
import { SocialSharingProvider } from "../providers/social-sharing/social-sharing";
import { ApprateProvider } from "../providers/apprate/apprate";
// import { BrowserService } from "../providers/browser-service/browser-service.service";

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) =>
          new TranslateHttpLoader(http, "./assets/i18n/", ".json"),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    AdMobFree,
    AppAvailability,
    YoutubeVideoPlayer,
    Market,
    FlurryAnalytics,
    SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AdmobProvider,
    UtilsProvider,
    MoviesDataProvider,
    Globalization,
    LangTagProvider,
    FavoritesProvider,
    FlurryProvider,
    SocialSharingProvider,
    AppRate,
    ApprateProvider,
    EmailComposer,
    AndroidPermissions,
    InAppBrowser,
    HTTP,
    // BrowserService
  ]
})
export class AppModule {}
