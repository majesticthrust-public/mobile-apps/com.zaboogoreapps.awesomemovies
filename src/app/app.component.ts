import { Component } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AdmobProvider } from "../providers/adsprovider/adsprovider";
import { TranslateService } from "@ngx-translate/core";
import { LangTagProvider } from "../providers/lang-tag/lang-tag";
import { appConfig } from "../app-config";
import { ApprateProvider } from "../providers/apprate/apprate";
// import { BrowserService } from "../providers/browser-service/browser-service.service";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  public rootPage = "MenuPage";

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    admobProvider: AdmobProvider,
    translate: TranslateService,
    langTag: LangTagProvider,
    appRate: ApprateProvider,
    // browserService: BrowserService
  ) {
    platform.ready().then(() => {
      // statusBar.styleDefault();
      statusBar.hide();
      splashScreen.hide();
      admobProvider.showBanner();

      translate.setDefaultLang("en");
      translate.use(langTag.preferredLanguage);

      appRate.promptForRating(appConfig.rateDialog.promptImmediately);

      // browserService.run();
    });
  }
}
