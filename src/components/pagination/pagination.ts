import { Component, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "pagination",
  templateUrl: "pagination.html",
})
export class PaginationComponent {
  @Input() public prevPageEnabled: boolean;
  @Input() public nextPageEnabled: boolean;

  @Output() public prevPage = new EventEmitter<void>();
  @Output() public nextPage = new EventEmitter<void>();

  constructor() {}
}
