import { Component, Input, EventEmitter, Output } from "@angular/core";
import { IMovieListResultObject, IMovieInfoObject } from "../../providers/movies-data/movies-data";
import { ModalController } from "ionic-angular";
import { appConfig } from "../../app-config";

/**
 * A list of movies.
 */
@Component({
  selector: "movie-list",
  templateUrl: "movie-list.html",
})
export class MovieListComponent {
  /**
   * A list of movies to display.
   */
  // TODO differentiate types? change behaviour based on type?
  @Input() public movies: IMovieListResultObject[] | IMovieInfoObject[];

  @Output() public onMovieClick = new EventEmitter<void>();

  public blurTMDBImages = appConfig.misc.blurTMDBImages;

  constructor(private modalCtrl: ModalController) {}

  public openModal(movie: IMovieListResultObject) {
    this.onMovieClick.emit();

    // TODO change modal according to these navparams
    const modalPage = this.modalCtrl.create("MovieDetailsPage", { movie });
    modalPage.present();
  }
}
