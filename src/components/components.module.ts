import { NgModule } from "@angular/core";
import { MovieListComponent } from "./movie-list/movie-list";
import { PaginationComponent } from "./pagination/pagination";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "ionic-angular";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [MovieListComponent, PaginationComponent],
  imports: [CommonModule, FormsModule, IonicModule, TranslateModule.forChild()],
  exports: [MovieListComponent, PaginationComponent],
})
export class ComponentsModule {}
