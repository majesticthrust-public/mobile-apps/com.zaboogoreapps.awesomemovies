import { MoviesListBasePage } from "./movies-list-page-base";
import {
  IonicPage,
  ModalController,
  LoadingController,
  NavController,
} from "ionic-angular";
import { Component } from "@angular/core";
import { MoviesDataProvider } from "../../providers/movies-data/movies-data";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { FlurryProvider } from "../../providers/flurry/flurry";

@IonicPage()
@Component({
  selector: "page-playing-now-movies-list",
  templateUrl: "movies-list-page.html",
})
export class PlayingNowMoviesListPage extends MoviesListBasePage {
  constructor(
    navController: NavController,
    moviesDataProvider: MoviesDataProvider,
    ads: AdmobProvider,
    modalCtrl: ModalController,
    loadingController: LoadingController,
    flurry: FlurryProvider
  ) {
    super(navController, moviesDataProvider, ads, modalCtrl, loadingController, flurry);
    this.listType = "playing-now";
  }
}
