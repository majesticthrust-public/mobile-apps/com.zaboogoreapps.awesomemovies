import { MoviesListBasePage } from "./movies-list-page-base";
import {
  IonicPage,
  ModalController,
  LoadingController,
  NavController,
} from "ionic-angular";
import { Component } from "@angular/core";
import { MoviesDataProvider } from "../../providers/movies-data/movies-data";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { FlurryProvider } from "../../providers/flurry/flurry";

@IonicPage()
@Component({
  selector: "page-popular-movies-list",
  templateUrl: "movies-list-page.html",
})
export class PopularMoviesListPage extends MoviesListBasePage {
  constructor(
    navController: NavController,
    moviesDataProvider: MoviesDataProvider,
    ads: AdmobProvider,
    modalCtrl: ModalController,
    loadingController: LoadingController,
    flurry: FlurryProvider
  ) {
    super(navController, moviesDataProvider, ads, modalCtrl, loadingController, flurry);
    this.listType = "popular";
  }
}
