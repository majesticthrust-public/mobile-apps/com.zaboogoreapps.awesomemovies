import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PlayingNowMoviesListPage } from "./playing-now-movies-list";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [PlayingNowMoviesListPage],
  imports: [
    IonicPageModule.forChild(PlayingNowMoviesListPage),
    ComponentsModule,
    TranslateModule.forChild(),
  ],
  exports: [PlayingNowMoviesListPage],
})
export class PlayingNowMoviesListPageModule {}
