import { ViewChild } from "@angular/core";
import {
  ModalController,
  Content,
  LoadingController,
  NavController
} from "ionic-angular";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import {
  MoviesDataProvider,
  IMovieListResultObject
} from "../../providers/movies-data/movies-data";
import { AntiPaginationAdapter } from "../../providers/movies-data/anti-pagination";
import { appConfig } from "../../app-config";
import { FlurryProvider } from "../../providers/flurry/flurry";

/**
 * Page base for specific movie lists: popular, new, playing now
 */
// TODO remake with observables
export class MoviesListBasePage {
  public get nextPageEnabled() {
    // unreliable; if total available movies is a multiple of results per page,
    // next page can be empty
    // TODO fix
    return this.movies.length === appConfig.app.resultsPerPage;
  }

  public get prevPageEnabled() {
    return this.listOffset > 0;
  }

  @ViewChild(Content) public content: Content;

  public toggled: boolean;
  public movies: IMovieListResultObject[];

  /**
   * Movie list type, one of `popular`, `new`, `playing-now`.
   * @constant
   */
  protected listType: string;

  protected movieRequester: AntiPaginationAdapter<IMovieListResultObject>;
  protected listOffset: number;

  constructor(
    protected navController: NavController,
    protected moviesDataProvider: MoviesDataProvider,
    protected ads: AdmobProvider,
    protected modalCtrl: ModalController,
    protected loadingController: LoadingController,
    protected flurry: FlurryProvider
  ) {
    this.listOffset = 0;
    this.movies = [];
  }

  public ionViewDidEnter() {
    this.flurry.startTimedEvent("Movies List", { page: this.listType });
  }

  public ionViewWillLeave() {
    this.flurry.endTimedEvent("Movies List", { page: this.listType });
  }

  public ionViewDidLoad() {
    this.movieRequester = this.createPaginationAdapter();

    // request movies
    this.requestMovies(this.listOffset).then(movies => {
      this.movies = movies;
    });

    // this.initializeCrossPromo();

    this.toggled = false;
  }

  public openSearch() {
    this.flurry.logEvent("Movies List: Open Search", { page: this.listType });
    this.navController.push("SearchResultsPage");
  }

  public async nextPage() {
    this.flurry.logEvent("Movies List: Next Page", { page: this.listType });

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    const newOffset = this.listOffset + appConfig.app.resultsPerPage;

    const movies = await this.requestMovies(newOffset);

    if (movies.length > 0) {
      this.movies = movies;
      this.listOffset = newOffset;
      this.content.scrollToTop();
    }
  }
  public async prevPage() {
    this.flurry.logEvent("Movies List: Prev Page", { page: this.listType });

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    const newOffset = Math.max(
      this.listOffset - appConfig.app.resultsPerPage,
      0
    );

    const movies = await this.requestMovies(newOffset);

    if (movies.length > 0) {
      this.movies = movies;
      this.listOffset = newOffset;
      this.content.scrollToTop();
    }
  }

  public onMovieClick() {
    this.flurry.logEvent("Movies List: Open Movie Details", {
      page: this.listType
    });
  }

  protected async requestMovies(offset: number) {
    const loader = this.loadingController.create({
      showBackdrop: true
    });
    loader.present();

    let movies = [];

    try {
      movies = await this.movieRequester.get(
        offset,
        appConfig.app.resultsPerPage
      );
    } finally {
      loader.dismiss();
    }

    return movies;
  }

  protected createPaginationAdapter() {
    let getMoviesFunc;
    switch (this.listType) {
      case "popular":
        getMoviesFunc = this.moviesDataProvider.miscPopularMovies;
        break;

      case "new":
        getMoviesFunc = this.moviesDataProvider.miscUpcomingMovies;
        break;

      case "playing-now":
        getMoviesFunc = this.moviesDataProvider.miscNowPlayingMovies;
        break;

      default:
        throw new Error(`Invalid movies list type: "${this.listType}".`);
    }

    return new AntiPaginationAdapter(async page => {
      const result = await getMoviesFunc({ page });
      const movies = result.results as IMovieListResultObject[];

      // prepare images
      return movies.map(movie => {
        // tslint:disable-next-line:variable-name
        const poster_path = this.moviesDataProvider.posterUrl(
          movie.poster_path,
          "w500"
        );

        // TODO backdrop url?

        return {
          ...movie,
          poster_path
        };
      });
    });
  }

  // protected async initializeCrossPromo() {
  //   this.utils.loaderCreate();
  //   this.utils.loaderPresent();

  //   const crossPromoData = await this.ads.requestApi();
  //   if (crossPromoData) {
  //     this.apps = crossPromoData.items
  //       // TODO change shuffle algorithm
  //       .sort(() => 0.5 - Math.random())
  //       .slice(0, 1);
  //   }

  //   this.utils.loaderDismiss();
  // }
}
