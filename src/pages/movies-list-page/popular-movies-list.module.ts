import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PopularMoviesListPage } from "./popular-movies-list";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [PopularMoviesListPage],
  imports: [
    IonicPageModule.forChild(PopularMoviesListPage),
    ComponentsModule,
    TranslateModule.forChild(),
  ],
  exports: [PopularMoviesListPage],
})
export class PopularMoviesListPageModule {}
