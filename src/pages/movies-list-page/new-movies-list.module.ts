import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { NewMoviesListPage } from "./new-movies-list";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [NewMoviesListPage],
  imports: [
    IonicPageModule.forChild(NewMoviesListPage),
    ComponentsModule,
    TranslateModule.forChild(),
  ],
  exports: [NewMoviesListPage],
})
export class NewMoviesListPageModule {}
