import { Component, ViewChild } from "@angular/core";
import { IonicPage, Nav, MenuController, Tabs } from "ionic-angular";
import { FlurryProvider } from "../../providers/flurry/flurry";

// Made according to this: https://devdactic.com/ionic-side-menu-tabs/
// Ionic 4 tutorial is somewhere in the comments

interface IPage {
  title: string;
  pageName: string;
  flurryName: string;
  flurryParams?: object;
  tabComponent?: any;
  index?: number;
  icon: string;
}

interface IPageGroup {
  name?: string;
  pages: IPage[];
}

const pageGroups: IPageGroup[] = [
  {
    pages: [
      {
        title: "menu.page-groups.primary.favorite",
        pageName: "FavoritesPage",
        flurryName: "Favorites",
        icon: "heart-outline"
      }
    ]
  },
  {
    name: "menu.page-groups.discover.title",
    pages: [
      {
        title: "menu.page-groups.discover.popular",
        pageName: "MovieListsTabsPage",
        flurryName: "Movies List",
        flurryParams: { page: "popular" },
        tabComponent: "PopularMoviesListPage",
        index: 0,
        icon: "star"
      },
      {
        title: "menu.page-groups.discover.new",
        pageName: "MovieListsTabsPage",
        flurryName: "Movies List",
        flurryParams: { page: "new" },
        tabComponent: "NewMoviesListPage",
        index: 1,
        icon: "information-circle"
      },
      {
        title: "menu.page-groups.discover.playing-now",
        pageName: "MovieListsTabsPage",
        flurryName: "Movies List",
        flurryParams: { page: "playing-now" },
        tabComponent: "PlayingNowMoviesListPage",
        index: 2,
        icon: "time"
      }
    ]
  },
  {
    name: "menu.page-groups.find.title",
    pages: [
      {
        title: "menu.page-groups.find.search",
        pageName: "SearchResultsPage",
        flurryName: "Search Results",
        icon: "search"
      },
      {
        title: "menu.page-groups.find.genres",
        pageName: "GenresListPage",
        flurryName: "Genres List",
        icon: "list"
      }
    ]
  }
];

@IonicPage()
@Component({
  selector: "page-menu",
  templateUrl: "menu.html"
})
export class MenuPage {
  public rootPage = "MovieListsTabsPage";

  public pageGroups: IPageGroup[] = pageGroups;

  @ViewChild(Nav) private nav: Nav;

  constructor(
    private menuController: MenuController,
    private flurry: FlurryProvider
  ) {}

  public async openPage(page: IPage) {
    this.flurry.logEvent(`Menu: Open ${page.flurryName}`, page.flurryParams);

    let params = {};

    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }

    // getting active nav according to how the now deprecated getActiveChildNav used to work
    // tslint:disable-next-line:max-line-length
    // https://github.com/ionic-team/ionic/blob/d22d77b48546de79e47b5dde2df1a9dac24c2e5c/src/navigation/nav-controller-base.ts#L1198

    // The active child nav is our Tabs Navigation
    const childNavs = this.nav.getActiveChildNavs();
    if (childNavs.length > 0 && page.index !== undefined) {
      const childNav = childNavs[childNavs.length - 1];
      await (childNav as Tabs).select(page.index);
    } else {
      // Tabs are not active, so reset the root page
      await this.nav.setRoot(page.pageName, params);
    }

    this.menuController.close();
  }

  public isActive(page: IPage) {
    // Again the Tabs Navigation
    const childNavs = this.nav.getActiveChildNavs();

    if (childNavs.length > 0) {
      const childNav = childNavs[childNavs.length - 1];
      if (
        childNav.getSelected() &&
        childNav.getSelected().root === page.tabComponent
      ) {
        return "secondary";
      }
      return;
    }
    // Fallback needed when there is no active childnav (tabs not active)
    // tslint:disable-next-line:one-line
    else if (
      this.nav.getActive() &&
      this.nav.getActive().name === page.pageName
    ) {
      return "secondary";
    } else {
      return;
    }
  }

  public showFavorites() {
    this.nav.setRoot("FavoritesPage");
  }
}
