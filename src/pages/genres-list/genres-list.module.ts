import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { GenresListPage } from "./genres-list";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [GenresListPage],
  imports: [
    IonicPageModule.forChild(GenresListPage),
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
  ],
})
export class GenresListPageModule {}
