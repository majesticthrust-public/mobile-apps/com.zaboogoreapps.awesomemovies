import { Component } from "@angular/core";
import { IonicPage, NavController, LoadingController } from "ionic-angular";
import {
  MoviesDataProvider,
  IGenre,
  isTMDBRequestError,
} from "../../providers/movies-data/movies-data";
import { Observable, BehaviorSubject } from "rxjs";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { FlurryProvider } from "../../providers/flurry/flurry";

@IonicPage()
@Component({
  selector: "page-genres-list",
  templateUrl: "genres-list.html",
})
export class GenresListPage {
  public filterQuery: string;
  public genres: Observable<IGenre[]>;
  private filter: BehaviorSubject<string>;

  constructor(
    private navCtrl: NavController,
    private moviesData: MoviesDataProvider,
    private loadingController: LoadingController,
    private ads: AdmobProvider,
    private flurry: FlurryProvider
  ) {
    this.filter = new BehaviorSubject("");
  }

  public ionViewDidEnter() {
    this.flurry.startTimedEvent("Genres List");
  }

  public ionViewWillLeave() {
    this.flurry.endTimedEvent("Genres List");
  }

  public ionViewDidLoad() {
    // load data from tmdb
    const tmdbGenres = Observable.fromPromise(this.moviesData.genreMovieList())
      .map((result) => {
        if (isTMDBRequestError(result)) {
          console.error(result);
          return [];
        } else {
          return result.genres as IGenre[];
        }
      })
      .shareReplay(1);

    // create filtered data stream
    this.genres = Observable.combineLatest(tmdbGenres, this.filter).map(
      ([genres, filterQuery]) =>
        genres.filter((genre) =>
          genre.name.includes(filterQuery.toLocaleLowerCase()),
        ),
    );

    const loading = this.loadingController.create({ showBackdrop: true });
    loading.present();
    this.genres.take(1).subscribe(() => loading.dismiss());
  }

  public updateList() {
    this.flurry.logEvent("Genres List: Searching");

    this.filter.next(this.filterQuery);
  }

  public openGenre(genre: IGenre) {
    this.flurry.logEvent("Genres List: Open Genre");

    this.ads.showInterstitial();
    this.navCtrl.push("GenreMoviesListPage", { genre });
  }
}
