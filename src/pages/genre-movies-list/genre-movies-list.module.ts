import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { GenreMoviesListPage } from "./genre-movies-list";
import { CommonModule } from "@angular/common";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [GenreMoviesListPage],
  imports: [
    IonicPageModule.forChild(GenreMoviesListPage),
    CommonModule,
    ComponentsModule,
    TranslateModule.forChild(),
  ],
})
export class GenreMoviesListPageModule {}
