import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavParams,
  Content,
  LoadingController,
} from "ionic-angular";
import {
  IGenre,
  MoviesDataProvider,
  IMovieListResultObject,
  isTMDBRequestError,
} from "../../providers/movies-data/movies-data";
import { AntiPaginationAdapter } from "../../providers/movies-data/anti-pagination";
import { BehaviorSubject, Observable } from "rxjs";
import { appConfig } from "../../app-config";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { FlurryProvider } from "../../providers/flurry/flurry";

@IonicPage()
@Component({
  selector: "page-genre-movies-list",
  templateUrl: "genre-movies-list.html",
})
export class GenreMoviesListPage {
  @ViewChild(Content) public content: Content;

  public genre: IGenre;
  public movies: Observable<IMovieListResultObject[]>;

  public nextPageEnabled: Observable<boolean>;
  public prevPageEnabled: Observable<boolean>;

  private movieRequester: AntiPaginationAdapter<IMovieListResultObject>;
  private listOffset: BehaviorSubject<number>;
  private totalResults: BehaviorSubject<number>;

  constructor(
    private navParams: NavParams,
    private moviesData: MoviesDataProvider,
    private ads: AdmobProvider,
    private loadingController: LoadingController,
    private flurry: FlurryProvider
  ) {
    this.genre = this.navParams.get("genre") as IGenre;

    this.listOffset = new BehaviorSubject(0);
    this.totalResults = new BehaviorSubject(0);

    this.movieRequester = new AntiPaginationAdapter(async (page) => {
      const result = await this.moviesData.discoverMovie({
        page,
        with_genres: `${this.genre.id}`,
      });
      console.log(result);

      if (isTMDBRequestError(result)) {
        console.error(result);
        return [];
      } else {
        // bad way to do this, but whatever
        this.totalResults.next(result.total_results);

        return result.results;
      }
    });

    this.initMovies();
    this.initPagination();
    this.initDebug();

    // loading indicator
    const loader = this.loadingController.create({
      showBackdrop: true,
      duration: 5000,
    });
    loader.present();
    this.movies
      .take(1)
      .subscribe(() => loader.dismiss());
  }

  public ionViewDidEnter() {
    this.flurry.startTimedEvent("Genre Movies List");
  }

  public ionViewWillLeave() {
    this.flurry.endTimedEvent("Genre Movies List");
  }

  public async nextPage() {
    this.flurry.logEvent("Genre Movies List: Next Page");

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    this.listOffset.take(1).subscribe((offset) => {
      const newOffset = offset + appConfig.app.resultsPerPage;
      this.listOffset.next(newOffset);
      this.content.scrollToTop();
    });
  }

  public async prevPage() {
    this.flurry.logEvent("Genre Movies List: Prev Page");

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    this.listOffset.take(1).subscribe((offset) => {
      const newOffset = Math.max(offset - appConfig.app.resultsPerPage, 0);
      this.listOffset.next(newOffset);
      this.content.scrollToTop();
    });
  }

  public onMovieClick() {
    this.flurry.logEvent("Genre Movies List: Open Movie Details");
  }

  private initMovies() {
    this.movies = this.listOffset
      // load movie info
      .mergeMap((offset) => {
        const p = this.movieRequester.get(offset, appConfig.app.resultsPerPage);
        return Observable.fromPromise(p);
      })

      // set image paths
      .map((movies) =>
        movies.map((movie) => {
          // tslint:disable-next-line:variable-name
          const poster_path = this.moviesData.posterUrl(
            movie.poster_path,
            "w500",
          );

          return {
            ...movie,
            poster_path,
          };
        }),
      )
      .shareReplay(1);
  }

  private initPagination() {
    this.nextPageEnabled = Observable.combineLatest(
      this.listOffset,
      this.totalResults,
    ).map(
      ([offset, total]) => offset + appConfig.app.resultsPerPage - 1 < total,
    );

    this.prevPageEnabled = this.listOffset.map((offset) => offset > 0);
  }

  private initDebug() {
    this.listOffset.subscribe((offset) =>
      console.log("GenreMoviesListPage new offset:", offset),
    );

    this.movies.subscribe((movies) =>
      console.log("GenreMoviesListPage new movies batch:", movies),
    );

    Observable.combineLatest(
      this.prevPageEnabled,
      this.nextPageEnabled,
    ).subscribe(([prev, next]) =>
      console.log(`Prev page enabled: ${prev}; next page enabled: ${next}`),
    );
  }
}
