import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { SearchResultsPage } from "./search-results";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [SearchResultsPage],
  imports: [
    IonicPageModule.forChild(SearchResultsPage),
    ComponentsModule,
    TranslateModule.forChild(),
  ],
})
export class SearchResultsPageModule {}
