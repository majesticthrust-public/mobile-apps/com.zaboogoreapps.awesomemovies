import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Content,
  LoadingController,
} from "ionic-angular";
import { BehaviorSubject, Observable } from "rxjs";
import {
  IMovieListResultObject,
  MoviesDataProvider,
  isTMDBRequestError,
} from "../../providers/movies-data/movies-data";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { appConfig } from "../../app-config";
import { AntiPaginationAdapter } from "../../providers/movies-data/anti-pagination";
import { FlurryProvider } from "../../providers/flurry/flurry";

@IonicPage()
@Component({
  selector: "page-search-results",
  templateUrl: "search-results.html",
})
export class SearchResultsPage {
  @ViewChild(Content) public content: Content;

  public nextPageEnabled: Observable<boolean>;
  public prevPageEnabled: Observable<boolean>;

  public movies: Observable<IMovieListResultObject[]>;

  public searchQuery: string;
  private search: BehaviorSubject<string>;
  private movieRequester: AntiPaginationAdapter<IMovieListResultObject>;

  private listOffset: BehaviorSubject<number>;
  private totalResults: BehaviorSubject<number>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private moviesData: MoviesDataProvider,
    private ads: AdmobProvider,
    private loadingController: LoadingController,
    private flurry: FlurryProvider
  ) {
    this.search = new BehaviorSubject("");
    this.listOffset = new BehaviorSubject(0);
    this.totalResults = new BehaviorSubject(0);

    this.movieRequester = this.createPaginationAdapter(this.searchQuery);

    this.initMovies();
    this.initPagination();
    this.initDebug();

    // loading indicator
    // TODO show non-blocking loading indicator during search
    // const loader = this.loadingController.create({
    //   showBackdrop: true,
    //   duration: 5000,
    // });
    // loader.present();
    // this.movies.take(1).subscribe(() => loader.dismiss());
  }

  public ionViewDidEnter() {
    this.flurry.startTimedEvent("Search Results");
  }

  public ionViewWillLeave() {
    this.flurry.endTimedEvent("Search Results");
  }

  public updateList() {
    this.flurry.logEvent("Search Results: Searching");

    this.movieRequester = this.createPaginationAdapter(this.searchQuery);
    this.search.next(this.searchQuery);
  }

  public async nextPage() {
    this.flurry.logEvent("Search Results: Next Page");

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    this.listOffset.take(1).subscribe((offset) => {
      const newOffset = offset + appConfig.app.resultsPerPage;
      this.listOffset.next(newOffset);
      this.content.scrollToTop();
    });
  }

  public async prevPage() {
    this.flurry.logEvent("Search Results: Prev Page");

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    this.listOffset.take(1).subscribe((offset) => {
      const newOffset = Math.max(offset - appConfig.app.resultsPerPage, 0);
      this.listOffset.next(newOffset);
      this.content.scrollToTop();
    });
  }

  public onMovieClick() {
    this.flurry.logEvent("Search Results: Open Movie Details");
  }

  private initMovies() {
    this.movies = Observable.combineLatest(this.search, this.listOffset)
      // load movie info
      .mergeMap(([_, offset]) => {
        const p = this.movieRequester.get(offset, appConfig.app.resultsPerPage);
        return Observable.fromPromise(p);
      })

      // set image paths
      .map((movies) =>
        movies.map((movie) => {
          // tslint:disable-next-line:variable-name
          const poster_path = this.moviesData.posterUrl(
            movie.poster_path,
            "w500",
          );

          return {
            ...movie,
            poster_path,
          } as IMovieListResultObject;
        }),
      )
      .shareReplay(1);
  }

  private initPagination() {
    this.nextPageEnabled = Observable.combineLatest(
      this.listOffset,
      this.totalResults,
    ).map(
      ([offset, total]) => offset + appConfig.app.resultsPerPage - 1 < total,
    );

    this.prevPageEnabled = this.listOffset.map((offset) => offset > 0);
  }

  private initDebug() {
    this.listOffset.subscribe((offset) =>
      console.log("SearchResultsPage new offset:", offset),
    );

    this.movies.subscribe((movies) =>
      console.log("SearchResultsPage new movies batch:", movies),
    );

    Observable.combineLatest(
      this.prevPageEnabled,
      this.nextPageEnabled,
    ).subscribe(([prev, next]) =>
      console.log(`Prev page enabled: ${prev}; next page enabled: ${next}`),
    );
  }

  /**
   * Create a new pagination adapter according to the current search query
   */
  private createPaginationAdapter(searchQuery: string) {
    return new AntiPaginationAdapter(async (page) => {
      if (!searchQuery || searchQuery.length === 0) {
        return [];
      }

      const result = await this.moviesData.searchMovie({
        query: searchQuery,
        page,
      });

      if (isTMDBRequestError(result)) {
        console.error(result);
        return [];
      } else {
        this.totalResults.next(result.total_results);

        return result.results;
      }
    });
  }
}
