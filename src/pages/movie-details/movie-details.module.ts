import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MovieDetailsPage } from "./movie-details";
import { TranslateModule } from "@ngx-translate/core";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [MovieDetailsPage],
  imports: [
    CommonModule,
    IonicPageModule.forChild(MovieDetailsPage),
    TranslateModule.forChild(),
  ],
})
export class MovieDetalsPageModule {}
