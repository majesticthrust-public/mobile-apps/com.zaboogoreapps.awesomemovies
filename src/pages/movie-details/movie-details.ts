import { Component } from "@angular/core";
import {
  IonicPage,
  NavParams,
  ViewController,
  NavController,
  LoadingController
} from "ionic-angular";
import { YoutubeVideoPlayer } from "@ionic-native/youtube-video-player";
/// import { SocialSharing } from '@ionic-native/social-sharing';
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { UtilsProvider } from "../../providers/utils/utils";
import {
  IMovieListResultObject,
  IMovieInfoObject,
  IVideoObject,
  MoviesDataProvider,
  isTMDBRequestError,
  IGenre
} from "../../providers/movies-data/movies-data";
import { FavoritesProvider } from "../../providers/favorites/favorites";
import { Observable } from "rxjs";
import { appConfig } from "../../app-config";
import { FlurryProvider } from "../../providers/flurry/flurry";
import { SocialSharingProvider } from "../../providers/social-sharing/social-sharing";
import { first } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-movie-details",
  templateUrl: "movie-detals.html"
})
export class MovieDetailsPage {
  public isFavorite: Observable<boolean>;
  public movie: Observable<IMovieInfoObject>;
  public videos: Observable<IVideoObject[]>;
  public trailer: Observable<IVideoObject | null>;

  public blurTMDBImages = appConfig.misc.blurTMDBImages;

  constructor(
    private ads: AdmobProvider,
    private utils: UtilsProvider,
    private navParams: NavParams,
    private navController: NavController,
    private viewCtrl: ViewController,
    private favorites: FavoritesProvider,
    private youtube: YoutubeVideoPlayer, // private socialSharing: SocialSharing
    private moviesData: MoviesDataProvider,
    private flurry: FlurryProvider,
    private social: SocialSharingProvider,
    private loading: LoadingController
  ) {}

  public ionViewDidEnter() {
    this.flurry.startTimedEvent("Movie Details");
  }

  public ionViewWillLeave() {
    this.flurry.endTimedEvent("Movie Details");
  }

  public async ionViewDidLoad() {
    const movie: IMovieListResultObject = this.navParams.get("movie");
    if (!movie) {
      console.error("MovieDetailsPage: no `movie` NavParam!");
    }

    this.ads.showInterstitial();

    this.isFavorite = this.favorites.isFavorite(movie.id);

    // get movie details
    this.movie = Observable.fromPromise(
      this.moviesData.movieInfo({
        id: movie.id
      })
    ).map(result => {
      if (isTMDBRequestError(result)) {
        return null;
      } else {
        // load images
        // tslint:disable-next-line:variable-name
        const backdrop_path = this.moviesData.backdropUrl(
          result.backdrop_path,
          "w780"
        );
        // tslint:disable-next-line:variable-name
        const poster_path = this.moviesData.posterUrl(
          result.poster_path,
          "w185"
        );

        // TODO get production companies logo urls

        return {
          ...result,
          backdrop_path,
          poster_path
        };
      }
    });

    // get videos related to the movie
    this.videos = Observable.fromPromise(
      this.moviesData.movieVideos({
        id: movie.id
      })
    ).map(result => (isTMDBRequestError(result) ? null : result.results));

    // get single trailer
    // TODO support displaying and playing all videos
    // TODO support different video hostings
    this.trailer = this.videos.map(videos => {
      videos.filter(v => v.site === "YouTube" && v.type === "Trailer");
      // take last trailer, if exists
      return videos.length > 0 ? videos[videos.length - 1] : null;
    });
  }

  public async addToFavorites() {
    this.flurry.logEvent("Movie Details: Added To Favories");

    this.movie.subscribe(movie => {
      this.favorites.add(movie.id);
    });
  }

  public removeFromFavorites() {
    this.flurry.logEvent("Movie Details: Removed From Favories");

    this.movie.subscribe(movie => {
      this.favorites.remove(movie.id);
    });
  }

  public share() {
    this.flurry.logEvent("Movie Details: Share");

    this.movie.pipe(first()).subscribe(async movie => {
      const loader = this.loading.create();
      loader.present();

      try {
        await this.social.shareMovie(movie.id);
      } catch (e) {
        console.error(e);
      } finally {
        loader.dismiss();
      }
    });
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  public async playVideo(video: IVideoObject) {
    this.flurry.logEvent("Movie Details: Play Video");

    try {
      await this.utils.checkYoutubeInstalled();
      this.youtube.openVideo(video.key);
    } catch (e) {
      this.utils.promptInstallYoutube();
    }
  }

  public async openGenre(genre: IGenre) {
    this.flurry.logEvent("Movie Details: Open Genre");

    this.navController.push("GenreMoviesListPage", { genre });
    // const result = await this.moviesData.genreMovieList();

    // if (!isTMDBRequestError(result)) {
    //   const genre = result.genres.find((g) => g.id === genreId);

    //   if (genre === undefined) {
    //     throw new Error(`Invalid genre id: ${genreId}`);
    //   }

    //   this.navController.push("GenreMoviesListPage", { genre });
    // }
  }
}
