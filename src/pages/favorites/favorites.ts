import { Component, ViewChild } from "@angular/core";
import { Content, IonicPage, LoadingController } from "ionic-angular";
import { AdmobProvider } from "../../providers/adsprovider/adsprovider";
import { appConfig } from "../../app-config";
import {
  MoviesDataProvider,
  IMovieInfoObject,
  isTMDBRequestError,
} from "../../providers/movies-data/movies-data";
import { FavoritesProvider } from "../../providers/favorites/favorites";
import { BehaviorSubject, Observable } from "rxjs";
import { FlurryProvider } from "../../providers/flurry/flurry";

@IonicPage()
@Component({
  selector: "page-favorites",
  templateUrl: "favorites.html",
})
export class FavoritesPage {
  public nextPageEnabled: Observable<boolean>;
  public prevPageEnabled: Observable<boolean>;

  @ViewChild(Content) public content: Content;

  public movies: Observable<IMovieInfoObject[]>;

  private favoriteMoviesIds: Observable<number[]>;
  private listOffset: BehaviorSubject<number>;

  constructor(
    private moviesDataProvider: MoviesDataProvider,
    private ads: AdmobProvider,
    private favorites: FavoritesProvider,
    private loadingController: LoadingController,
    private flurry: FlurryProvider
  ) {
    this.listOffset = new BehaviorSubject(0);
    this.movies = new Observable();

    // revery new page request requires a resubscription; replaying last value
    this.favoriteMoviesIds = this.favorites.getAllIds().shareReplay(1);

    // init movies observable
    this.initMovies();

    // init pagination enabled markers
    this.initPagination();

    // init subscriptions with debug messages
    this.initDebug();

    // loading indicator
    const loader = this.loadingController.create({
      showBackdrop: true,
    });
    loader.present();
    this.movies.take(1).subscribe(() => loader.dismiss());
  }

  public ionViewDidEnter() {
    this.flurry.startTimedEvent("Favorites");
  }

  public ionViewWillLeave() {
    this.flurry.endTimedEvent("Favorites");
  }

  public ionViewDidLoad() {
    // this.initializeCrossPromo();
  }

  public async nextPage() {
    this.flurry.logEvent("Favorites: Next Page");

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    this.listOffset.take(1).subscribe((offset) => {
      const newOffset = offset + appConfig.app.resultsPerPage;
      this.listOffset.next(newOffset);
      this.content.scrollToTop();
    });
  }

  public async prevPage() {
    this.flurry.logEvent("Favorites: Prev Page");

    // this.initializeCrossPromo();
    this.ads.showInterstitial();

    this.listOffset.take(1).subscribe((offset) => {
      const newOffset = Math.max(offset - appConfig.app.resultsPerPage, 0);
      this.listOffset.next(newOffset);
      this.content.scrollToTop();
    });
  }

  public onMovieClick() {
    this.flurry.logEvent("Favorites: Open Movie Details");
  }

  private initMovies() {
    this.movies = Observable.combineLatest(
      this.favoriteMoviesIds,
      this.listOffset,
    )
      // select the requested slice
      .map(([ids, offset]) =>
        ids.slice(offset, offset + appConfig.app.resultsPerPage),
      )

      // load movie info
      .mergeMap((ids) => {
        const p = Promise.all(
          ids.map((id) => this.moviesDataProvider.movieInfo({ id })),
        );
        return Observable.fromPromise(p);
      })

      // filter out any errors
      .map(
        (movies) =>
          movies.filter(
            (movie) => !isTMDBRequestError(movie),
          ) as IMovieInfoObject[],
      )

      // set image paths
      .map((movies) =>
        movies.map((movie) => {
          // tslint:disable-next-line:variable-name
          const poster_path = this.moviesDataProvider.posterUrl(
            movie.poster_path,
            "w780",
          );

          return {
            ...movie,
            poster_path,
          };
        }),
      )
      .shareReplay(1);
  }

  private initPagination() {
    this.nextPageEnabled = Observable.combineLatest(
      this.favoriteMoviesIds,
      this.listOffset,
    ).map(
      ([ids, offset]) => offset + appConfig.app.resultsPerPage - 1 < ids.length,
    );

    this.prevPageEnabled = this.listOffset.map((offset) => offset > 0);
  }

  private initDebug() {
    this.listOffset.subscribe((offset) =>
      console.log("FavoritesPage new offset:", offset),
    );

    this.favoriteMoviesIds.subscribe((ids) =>
      console.log("FavoritesPage new favorites batch:", ids),
    );

    this.movies.subscribe((movies) =>
      console.log("FavoritesPage new movies batch:", movies),
    );

    Observable.combineLatest(
      this.prevPageEnabled,
      this.nextPageEnabled,
    ).subscribe(([prev, next]) =>
      console.log(`Prev page enabled: ${prev}; next page enabled: ${next}`),
    );
  }
}
