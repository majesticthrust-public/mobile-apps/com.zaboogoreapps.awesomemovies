import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FavoritesPage } from "./favorites";
import { CommonModule } from "@angular/common";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [FavoritesPage],
  imports: [
    IonicPageModule.forChild(FavoritesPage),
    CommonModule,
    ComponentsModule,
    TranslateModule.forChild(),
  ],
})
export class FavoritesPageModule {}
