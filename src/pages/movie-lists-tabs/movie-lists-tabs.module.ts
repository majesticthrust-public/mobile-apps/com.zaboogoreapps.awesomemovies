import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MovieListsTabsPage } from "./movie-lists-tabs";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [MovieListsTabsPage],
  imports: [
    IonicPageModule.forChild(MovieListsTabsPage),
    TranslateModule.forChild(),
  ],
})
export class MovieListsTabsPageModule {}
