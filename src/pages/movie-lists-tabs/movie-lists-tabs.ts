import { Component } from "@angular/core";
import { IonicPage, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-movie-lists-tabs",
  templateUrl: "movie-lists-tabs.html",
})
export class MovieListsTabsPage {
  public popularMoviesRoot = "PopularMoviesListPage";
  public newMoviesRoot = "NewMoviesListPage";
  public playingNowMoviesRoot = "PlayingNowMoviesListPage";

  public myIndex: number;

  constructor(private navParams: NavParams) {
    this.myIndex = this.navParams.get("tabIndex") || 0;
  }
}
