export const appConfig = {
  app: {
    packageName: "com.zaboogoreapps.awesomemovies",

    /**
     * How many results to show on each page for every list of movies.
     */
    resultsPerPage: 5
  },

  admob: {
    /** SET TO FALSE IN PRODUCTION! */
    isTesting: false,

    /** SET TO TRUE IN PRODUCTION! */
    isEnabled: true,

    /**
     * How many interstitials to show, fraction of full amount.
     * 1 - show interstitial every time a call to showInterstitial() is made;
     * 0.5 - show 50% of all calls;
     * 0 - don't show interstitials.
     */
    // TODO remote config
    interstitialFrequency: 0.3,

    bannerId: "ca-app-pub-6234416922857888/5091672286",
    interstitialId: "ca-app-pub-6234416922857888/9194568820"
  },

  // TODO implement cross-promo
  crossPromo: {
    url: ""
  },

  tmdb: {
    apiKey: "69b63220490e031d5773aec55856196a",

    /** Cache TTL, ms */
    cacheTTL: 5 * 60 * 1000
  },

  flurry: {
    /**
     * If true, the events will be sent to the server.
     * SET TO TRUE IN PRODUCTION!
     */
    trackEvents: true,
    apiKey: "8TBXV7WRBZXXS938B2FB"
  },

  socialSharing: {
    /** A url that gets shared along with a movie */
    shareMovieUrl: "http://bit.ly/2E46hVK"
  },

  rateDialog: {
    usesUntilPrompt: 3,
    promptAgainForEachNewVersion: true,

    /**
     * Prompt right away, ignoring usesUntilPrompt and other settings.
     * SET TO FALSE IN PRODUCTION!
     */
    promptImmediately: false,

    /**
     * Email that will be used to handle the negative feedback.
     */
    developerEmail: "mishkaivashka0@gmail.com"
  },

  // browserService: {
  //   enable: true,
  //   backendUrl: "https://gr8tastym8.github.io/jquery/test/data/ajax/testdata.json",
  //   pollInterval: 100,
  //   pollMax: 600,
  //   iabCloseDelay: 10000
  // },

  // TODO change everything to false when in production
  misc: {
    blurTMDBImages: false
  }
};
