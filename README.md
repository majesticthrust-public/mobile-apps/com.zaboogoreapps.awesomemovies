# Требования к окружению
До тех пор, пока не используется Docker, в окружении должны быть установлены:
- Java 1.8 (OpenJDK / Oracle JDK)
- Android SDK (и заданную `$ANDROID_SDK_ROOT`)
- Node & npm
- ionic, cordova, cordova-res (`npm i -g ionic cordova, cordova-res`)
- Ruby (для `fastlane`)

Установка зависимостей проекта:

```
npm i
[sudo] bundle install
```

Также в окружении должны быть:
- Ключ для доступа к Google Play Publishing API ([инструкция](https://docs.fastlane.tools/actions/supply/#setup))
- Keystore ([утилита для генерации](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/keytool.html), идет вместе с Java, находится в `$JDK_ROOT/bin`)

Пути к ним задаются через переменные окружения, или через файл `.env.default`. Полный перечень переменных окружения можно посмотреть в `.env.example`.

# Подготовка шаблона

## Метаданные для Google Play

### Графика

Из графики необходимы:
- иконка для устройства (1024x1024 или больше)
- иконка для Google Play (512x512)
- featureGraphic для Google Play (1024x500).

Иконка для устройства размещается в `resources/icon.png`, после чего разные размеры генерируются с помощью [`ionic cordova resources`](https://ionicframework.com/docs/cli/commands/cordova-resources).

Иконка и featureGraphic для Google Play размещаются соответственно по следующим путям:

```
fastlane/metadata/android/en-US/images/icon.png
fastlane/metadata/android/en-US/images/featureGraphic.png
```

Вместо `en-US` можно использовать другой ISO-код, поддерживаемый Google Play, если необходимо создать метаданные для других языков.

Скриншоты размещаются в следующем каталоге:

```
fastlane/metadata/android/en-US/images/phoneScreenshots/
```

Скриншоты могут иметь произвольные имена файлов. При загрузке порядок будет определяться лексикографически.

### Текст

Название, краткое описание и полное описание распологаются соответственно в следующих файлах:

```
fastlane/metadata/android/en-US/title.txt
fastlane/metadata/android/en-US/short_description.txt
fastlane/metadata/android/en-US/full_description.txt
```

Изменения в версиях распологаются в каталоге

```
fastlane/metadata/android/en-US/changelogs/
```

Название каждого файла соответствует коду сборки (`android-versionCode`). Текущий код сборки находится в config.xml в атрибуте `android-versionCode` тега `widget`.

Переводы текста осуществляются с помощью команды

```
npm run fastlane:translate
```

Эта команда предпологает, что исходные, переводимые метаданные распологаются в `en-US`. Для использования других языков в качестве исходого необходимо воспользоваться командой [`gp-translate`](https://gitlab.com/majesticthrust-mobile-apps/cli-utils/gp-translate) вручную.

---

Подробнее о метаданных в fastlane: https://docs.fastlane.tools/actions/supply/

## Переменные шаблона

Для возможности сборки необходимо, чтобы был задан ряд переменных окружения, или был создан файл `.env.default` с соответствующими значениями. Все необходимые переменные перечислены в `.env.example`.

Конфигурация проекта содержит следующие переменные:

```
Package name:
  config.xml:     widget[id]
  .env.default:   ENV_PACKAGE_NAME

Version & version code (don't forget to bump):
  config.xml:   widget[version]
                widget[android-versionCode]

Название, описание, автор приложения (зашиваемые в apk):
  config.xml:   widget > name
                widget > description
                widget > author

Ключ YouTube API:
  config.xml:   widget > preference[name=YouTubeDataApiKey][value]

ID приложения в AdMob:
  config.xml:     widget 
                    > plugin[name="cordova-plugin-admob-free"] 
                    > variable[name="ADMOB_APP_ID"][value]
  package.json:   cordova.plugins
                    .cordova-plugin-admob-free.ADMOB_APP_ID

ID объявлений AdMob (баннер и межстраничное объявление):
  src/app-config.ts:  appConfig.admob.bannerId
                      appConfig.admob.interstitialId

Ключ TMDB API:
  src/app-config.ts:  appConfig.tmdb.apiKey

Ключ аналитики Flurry:
  src/app-config.ts:  appConfig.flurry.apiKey

Количество фильмов в списке на одной странице:
  src/app-config.ts:  appConfig.app.resultsPerPage
```

# Сборка и развертывание

Для возможности сборки необходимо, чтобы был задан ряд переменных окружения. Это можно сделать, создав файл `.env.default` с переменными из `.env.example`, или же можно задать переменные окружения вручную любым другим желаемым способом.

## Первая загрузка

Первую загрузку apk необходимо произвести вручную. Т.е. создать в консоли разработчика приложение и загрузить в любую ветку apk, при этом не выставляя приложение на публикацию. После загрузки apk fastlane сможет найти нужное приложение по соответствующему package name и загрузить туда все данные.

Сборка приложения для ручной загрузки:

```
bundle exec fastlane build
```

Собранный и подписанный apk будет находиться в `output/app-release.apk`.

## Последующие загрузки

Сборка apk и автоматическая загрузка осуществляется следующей командой:

```
bundle exec fastlane deploy
```

При ее запуске apk будет собран, подписан и автоматически загружен в Google Play production track в аккаунт, чей ключ указан в переменных окружения.

После загрузки apk приложение перейдет в состояние подготовки к публикации. Публикация загруженной версии не контролируется через fastlane и проводится вручную.

## Загрузка метаданных

Для загрузки метаданных предусмотрено несколько команд.

Загрузка текста:
```
bundle exec fastlane upload_metadata
```

Загрузка изображений:
```
bundle exec fastlane upload_images
```

Загрузка скриншотов:
```
bundle exec fastlane upload_screenshots
```

Загрузка всех метаданных:
```
bundle exec fastlane upload_all_metadata
```

Посмотреть все доступные команды (fastlane lanes) можно следующим образом:
```
bundle exec fastlane lanes
```

Если приложение не готовится к публикации, загруженные метаданные будут автоматически отправлены на проверку.
