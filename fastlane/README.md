fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android build
```
fastlane android build
```
Build release apk and move to 'output/''
### android deploy
```
fastlane android deploy
```
Build and upload apk
### android translate_metadata
```
fastlane android translate_metadata
```
Translate text metadata
### android upload_text_metadata
```
fastlane android upload_text_metadata
```
Upload text metadata
### android upload_images
```
fastlane android upload_images
```
Upload images
### android upload_screenshots
```
fastlane android upload_screenshots
```
Upload screenshots
### android validate
```
fastlane android validate
```
Validate
### android upload_all_metadata
```
fastlane android upload_all_metadata
```
Upload all metadata (text + images + screenshots) to Google Play
### android upload_apk_text
```
fastlane android upload_apk_text
```
Build apk and upload text metadata + apk to Google Play
### android upload_all
```
fastlane android upload_all
```
Build apk and upload everything to Google Play

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
