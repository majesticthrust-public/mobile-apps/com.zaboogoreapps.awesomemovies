# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  # Just build and sign an apk
  private_lane :build_release do
    ionic(
      platform: "android",
      release: true,
      prod: true,

      keystore_path: ENV["ENV_KEYSTORE_PATH"],
      keystore_password: ENV["ENV_KEYSTORE_PASSWORD"],
      keystore_alias: ENV["ENV_KEYSTORE_KEY_ALIAS"],
      key_password: ENV["ENV_KEYSTORE_KEY_PASSWORD"],
    )
  end

  desc "Build release apk and move to 'output/''"
  lane :build do
    build_release()

    copy_artifacts(
      target_path: "output",
      artifacts: [ENV["CORDOVA_ANDROID_RELEASE_BUILD_PATH"]]
    )
  end

  desc "Build and upload apk"
  lane :deploy do
    build_release()

    upload_to_play_store(
      apk: ENV["CORDOVA_ANDROID_RELEASE_BUILD_PATH"],
      skip_upload_aab: false,
      skip_upload_metadata: true,
      skip_upload_images: true,
      skip_upload_screenshots: true,
      skip_upload_changelogs: true
    )
  end

  desc "Translate text metadata"
  lane :translate_metadata do
    sh("npm", "run", "fastlane:translate")
    sh("npm", "run", "fastlane:fix")
  end

  desc "Upload text metadata"
  lane :upload_text_metadata do
    upload_to_play_store(
      skip_upload_apk: true,
      skip_upload_aab: true,
      skip_upload_metadata: false,
      skip_upload_images: true,
      skip_upload_screenshots: true,
      skip_upload_changelogs: true
    )
  end

  desc "Upload images"
  lane :upload_images do
    upload_to_play_store(
      skip_upload_apk: true,
      skip_upload_aab: true,
      skip_upload_metadata: true,
      skip_upload_images: false,
      skip_upload_screenshots: true,
      skip_upload_changelogs: true
    )
  end

  desc "Upload screenshots"
  lane :upload_screenshots do
    upload_to_play_store(
      skip_upload_apk: true,
      skip_upload_aab: true,
      skip_upload_metadata: true,
      skip_upload_images: true,
      skip_upload_screenshots: false,
      skip_upload_changelogs: true
    )
  end

  desc "Validate"
  lane :validate do
    upload_to_play_store(
      validate_only: true,
      skip_upload_changelogs: true,
      apk: ENV["CORDOVA_ANDROID_RELEASE_BUILD_PATH"],
    )
  end

  desc "Upload all metadata (text + images + screenshots) to Google Play"
  lane :upload_all_metadata do
    upload_to_play_store(
      skip_upload_apk: true,
      skip_upload_aab: true,
      skip_upload_metadata: false,
      skip_upload_images: false,
      skip_upload_screenshots: false,
      skip_upload_changelogs: true,
    )
  end

  desc "Build apk and upload text metadata + apk to Google Play"
  lane :upload_apk_text do
    build_release()

    upload_to_play_store(
      skip_upload_images: true,
      skip_upload_screenshots: true,
      skip_upload_changelogs: true,
      apk: ENV["CORDOVA_ANDROID_RELEASE_BUILD_PATH"],
    )
  end

  desc "Build apk and upload everything to Google Play"
  lane :upload_all do
    build_release()

    upload_to_play_store(
      skip_upload_changelogs: true,
      apk: ENV["CORDOVA_ANDROID_RELEASE_BUILD_PATH"],
    )
  end
end
